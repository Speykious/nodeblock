const { loadImage } = require("canvas");
const { drawSpectrum } = require("./draw-spectrum");
const { spectrumSettings, barSettings, circSettings } = require("./audio-settings");
const { getHeader, decodeAudioData } = require("./audio-read");
const roundRect = require("./roundRect");
const Viewport = require("./Viewport");
const noise = require("./noise");

async function load(_ctx, songFilename, bgImageFilename) {
  //const audioFileBuffer = readFileSync(songFilename);
  const { sampleRate } = getHeader(songFilename);
  
  const framerate = 60;
  const fftSize = 32768;
  const fftHalf = fftSize / 2;
  const step = sampleRate / framerate;
  //const totalFrames = audioBuffer.duration * framerate;

  const pcmData = new Float32Array(decodeAudioData(songFilename));
  const dataArray = [];
  const origoffset = 20;
  const morigoffset = origoffset * sampleRate / 1000;

  const bgImage = await loadImage(bgImageFilename);
  const nbImage = await loadImage("src/noteblock.png");
  const bgViewport = new Viewport({ image: bgImage, s: 1.2, a: 20 });
  const nbViewport = new Viewport({ image: nbImage, s: 0.5, a: 0 });

  return {
    sampleRate,
    fftHalf,
    step,
    pcmData, dataArray,
    morigoffset,
    bgViewport, nbViewport,
    seed1: [Math.random() * 42, Math.random() * 42],
    seed2: [Math.random() * 42, Math.random() * 42],
    seed3: [Math.random() * 42, Math.random() * 42],
  };
}

async function draw(ctx, frame, props) {
  const {
    sampleRate,
    fftHalf,
    step,
    pcmData, dataArray,
    morigoffset,
    bgViewport, nbViewport,
    seed1, seed2, seed3
  } = props;

  const index = frame * step;
  const noiseTick = frame / 50;

  const cw = ctx.canvas.width;
  const ch = ctx.canvas.height;
  const hcw = cw >> 1, hch = ch >> 1;

  // Anti-drawing-clutter safety :v
  ctx.fillStyle = "#161616";
  ctx.fillRect(0, 0, cw, ch);
  
  // Take the appropriate slice of raw PCM waveform
  const waveform = pcmData.slice(
    index - fftHalf + morigoffset,
    index + fftHalf + morigoffset
  );
  

  // Draw background image
  bgViewport.a = 20 * noise(noiseTick / 2, ...seed1) - 10;
  bgViewport.x = 50 * noise(noiseTick, ...seed2) - 25;
  bgViewport.y = 20 * noise(noiseTick, ...seed3) - 10;
  bgViewport.draw(ctx);
  
  // Dim background image
  ctx.fillStyle = "#00000030";
  ctx.fillRect(0, 0, cw, ch);

  // Draw spectrum (duh)
  drawSpectrum(
    ctx,
    { sampleRate },
    16384,
    waveform,
    dataArray,
    spectrumSettings,
    barSettings,
    circSettings
  );
  
  // Draw noteblock platform
  ctx.fillStyle = "#ffffffcc";
  roundRect(ctx, hcw, ch * 0.755, cw / 1.75, hch / 3, cw / 40).fill();
  
  // Draw noteblocks
  nbViewport.y = -hch * 0.675;
  for (let x = -3; x <= 3; x++) {
    nbViewport.x = x * nbViewport.image.width * nbViewport.s;
    nbViewport.draw(ctx);
  }
  
  return index < pcmData.length;
}

module.exports = { load, draw }
