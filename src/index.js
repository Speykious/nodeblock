const { spawn } = require("child_process");
const { createCanvas } = require("canvas");
const { load, draw } = require("./sketch");

// Adapted from the example code from [this page](https://nodejs.org/api/stream.html#stream_writable_write_chunk_encoding_callback)
const writeAsync = (stream, data) =>
  new Promise((resolve, _reject) => {
    if (!stream.write(data))
      stream.once("drain", resolve);
    else
      process.nextTick(resolve);
  });

const argv = process.argv.slice(1);

(async function () {
  const framerate = 60;
  const width = 1920, height = 1080;

  const canvas = createCanvas(width, height);
  const ctx = canvas.getContext("2d");
  ctx.quality = "best";

  if (argv.length < 3) {
    // argv[0] is USELESS. smh
    console.log(`Usage: node . <song> <image> [output]`);
    process.exit(1);
  }

  const songFilename = argv[1];
  const bgImageFilename = argv[2];
  const output = argv[3] ?? "media-bin/output.mp4";

  const ffmpipe = spawn(
    "ffmpeg",
    [
      "-hide_banner", "-y",
      "-f", "rawvideo",
      "-pix_fmt", "rgba",
      "-r", `${framerate}`,
      "-s", `${width}x${height}`,
      "-i", "-",
      "-i", songFilename,
      "-b:a", "192k",
      "-crf", "23",
      "-r", `${framerate}`,
      "-pix_fmt", "yuv420p",
      `${output}`
    ],
    { stdio: ["pipe", process.stdout, process.stderr] }
  );
  ffmpipe.on("close", code => {
    if (code) console.log("ffmpeg process failed with code", code);
    else console.log("\x1b[1m\x1b[33mffmpeg finished encoding!\x1b[0m");
  });
  
  const props = await load(ctx, songFilename, bgImageFilename);
  console.log("\x1b[1mProps:\x1b[0m", props);
  
  props.framerate = framerate;
  let nextDraw = true;
  console.log(ctx);
  for (let frame = 0; nextDraw; frame++) {
    ctx.save();
    nextDraw = await draw(ctx, frame, props);
    ctx.restore();
    const raw = new Uint8Array(ctx.getImageData(0, 0, width, height).data);
    process.stdout.write(`\x1b[32m\rWriting image ${frame}... \x1b[1m|\x1b[0m `);
    await writeAsync(ffmpipe.stdin, raw);
  }
  ffmpipe.stdin.end();
})();

