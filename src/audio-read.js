const { execSync } = require("child_process");

function getHeader(audioFilename) {
  const rawInfo = execSync(
    `ffprobe -hide_banner '${audioFilename}' 2>&1 | grep -A1 Duration:`,
    { encoding: "utf8" }
  ).split("\n");
  
  const matches = rawInfo[0].match(/Duration: (\d+):(\d+):([0-9.]+),/);
  const [_, hours, minutes, seconds] = matches.map(Number);
  const duration = seconds + 60 * minutes + 3600 * hours;
  
  const motches = rawInfo[1].match(/Audio: (\w+), (\d+) Hz,/);
  const extension = motches[1];
  const sampleRate = Number(motches[2]);
  
  return {
    hours, minutes, seconds, duration,
    extension, sampleRate
  };
}

function decodeAudioData(audioFilename) {
  const buffer = execSync(
    `ffmpeg -hide_banner -i '${audioFilename}' -ac 1 -f f32le -acodec pcm_f32le -`,
    { maxBuffer: 1024 * 1024 * 100 }
  );
  return buffer.buffer;
}

module.exports = { getHeader, decodeAudioData }
